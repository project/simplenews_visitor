<?php

/**
 * @file
 * Extends Simplenews by providing a visitor log by user.
 *
 * Date: ven 28 giu 2013, 10.10.53, CEST
 * File: simplenews_visitor.install
 * Author: stefano
 */

/**
 * Implements hook_schema().
 *
 * SC 25/mag/2011 18.05.42 stefano
 */
function simplenews_visitor_schema() {
  $schema['simplenews_visitor'] = array(
    'description' => 'Subscriber visit.',
    'fields'      => array(
      'nid'  => array(
        'description' => 'The primary identifier for a {node}.',
        'type'        => 'int',
        'not null'    => TRUE,
        'default'     => 0,
      ),
      'mail' => array(
        'description' => 'The subscriber email address.',
        'type'        => 'varchar',
        'length'      => 64,
        'not null'    => TRUE,
        'default'     => '',
      ),
      'url'  => array(
        'description' => 'The url visited.',
        'type'        => 'varchar',
        'length'      => 255,
        'not null'    => TRUE,
        'default'     => '',
      ),
      'link' => array(
        'description' => 'The url visited.',
        'type'        => 'varchar',
        'length'      => 255,
        'not null'    => TRUE,
        'default'     => '',
      ),
      'date' => array(
        'description' => 'The date.',
        'type'        => 'datetime',
        'mysql_type'  => 'DATETIME',
        'pgsql_type'  => 'timestamp without time zone',
        'sqlite_type' => 'VARCHAR',
        'sqlsrv_type' => 'smalldatetime',
        'not null'    => TRUE,
      ),
    ),
    'indexes'     => array(
      'mail_sn_vis' => array('mail'),
      'nid_sn_vis'  => array('nid'),
    ),
  );

  /*
   * Limit amount send email.
   *
   * SC 27/giu/2011 23:41:53 stefano
   */
  $schema['simplenews_visitor_max_sent'] = array(
    'description' => 'Email limit send',
    'fields'      => array(
      'tid'    => array(
        'description' => 'The primary identifier for a {term_data}.',
        'type'        => 'int',
        'not null'    => TRUE,
        'default'     => 0,
      ),
      'max'    => array(
        'description' => 'The max amount of email to send',
        'type'        => 'int',
        'not null'    => TRUE,
        'default'     => -1,
      ),
      'amount' => array(
        'description' => 'The amount of email sent',
        'type'        => 'int',
        'not null'    => TRUE,
        'default'     => 0,
      ),
    ),
    'indexes'     => array(
      'tid_sn_vis_count' => array('tid'),
    ),
    'primary key' => array('tid'),
  );

  return $schema;
}

/**
 * Implements hook_schema_alter().
 *
 * Added a new column to simplenews_snid_tid.
 *
 * SC 24/giu/2011 00.15.14 stefano
 */
function simplenews_visitor_schema_alter(&$schema) {
  $schema['simplenews_snid_tid']['fields']['activated'] = array(
    'description' => 'Boolean indicating the status of the subscription.',
    'type'        => 'int',
    'size'        => 'tiny',
    'not null'    => TRUE,
    'default'     => 1,
  );
}
